import React, { Component } from "react";
import "./App.css";
import DevTools from "mobx-react-devtools";
import HomePage from "./components/HomePage";
import { BrowserRouter as Router, Route } from "react-router-dom";
import AddTodo from "./components/AddTodo";

class App extends Component {
	render() {
		return (
			<Router>
				<div>
					<DevTools />
					<Route exact path="/" component={HomePage} />
					<Route path="/add" component={AddTodo} />
					<Route path="/todo/:id" component={AddTodo} />
				</div>
			</Router>
		);
	}
}

export default App;
