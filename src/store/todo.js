import { action, observable } from "mobx";
import axios from "axios";
import { URL } from "./../constants/apiConstants";
import { STATUS } from "../constants/constants";

class TasksStore {
	@observable
	todo = {
		tasks: []
	};

	@action
	getTasks = () => {
		axios
			.get(URL.BASE_URL + URL.tasks)
			.then(res => {
				res.data = res.data.sort((a, b) => {
					return a.modifiedDate - b.modifiedDate;
				});
				this.todo.tasks = [...[], ...res.data];
			})
			.catch(ex => {
				console.log(ex);
			});
	};

	toggleTaskStatus = taskToToggleStatus => {
		axios
			.put(URL.BASE_URL + URL.tasks + "/" + taskToToggleStatus.id, {
				status:
					taskToToggleStatus.status === STATUS.PENDING
						? STATUS.COMPLETED
						: STATUS.PENDING,
				createdDate: taskToToggleStatus.createdDate,
				modifiedDate: new Date(),
				name: taskToToggleStatus.name
			})
			.then(res => {
				this.todo = {
					...this.todo,
					tasks: this.todo.tasks.map(task => {
						if (task.id === taskToToggleStatus.id) {
							task.status =
								taskToToggleStatus.status === STATUS.PENDING
									? STATUS.COMPLETED
									: STATUS.PENDING;
						}
						return task;
					})
				};
			});
	};

	deleteTasks = taskToDelete => {
		axios
			.delete(`${URL.BASE_URL}${URL.tasks}/${taskToDelete.id}`)
			.then(res => {
				this.todo = {
					...this.todo,
					tasks: this.todo.tasks.filter(
						task => task.id !== taskToDelete.id
					)
				};
			})
			.catch(ex => {
				console.log(ex);
			});
	};

	addTasks = taskName => {
		return axios.post(URL.BASE_URL + URL.tasks, {
			name: taskName,
			createdDate: new Date().getTime(),
			status: STATUS.PENDING,
			modifiedDate: new Date()
		});
	};

	updateTasks = (taskName, currentTask) => {
		return axios.put(URL.BASE_URL + URL.tasks + "/" + currentTask.id, {
			status: currentTask.status,
			createdDate: currentTask.createdDate,
			modifiedDate: new Date(),
			name: taskName
		});
	};

	getTodoById = taskId => {
		return axios.get(URL.BASE_URL + URL.tasks + "/" + taskId);
	};
}

export default new TasksStore();
