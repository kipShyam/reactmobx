import React, { Component } from "react";
import { ListGroupItem, Row, Col, Button } from "reactstrap";
import { Link } from "react-router-dom";
import { STATUS } from "../constants/constants";

class ListItem extends Component {
	render() {
		return (
			<ListGroupItem className="text-left" key={this.props.index}>
				<Row>
					<Col sm={6}>{this.props.items.name}</Col>
					<Col sm={3}>{this.props.items.status || "---"}</Col>
					{/* <Col sm={2}>{new Date(this.props.items.modifiedDate).toUTCString()}</Col> */}
					<Col sm={3}>
						<Col sm={3}>
							<Link to={"/todo/" + this.props.items.id}>
								<Button
									color="success"
									title="Edit"
									disabled={
										this.props.items.status === STATUS.PENDING
											? false
											: true
									}
								>
									Edit
								</Button>
							</Link>
						</Col>
						<Col sm={5}>
							<Button
								color="warning"
								title={
									this.props.items.status === STATUS.PENDING
										? "COMPLETED"
										: "PENDING"
								}
								onClick={this.props.toggleCompleted}
							>
								{this.props.items.status === STATUS.PENDING
									? "COMPLETED"
									: "PENDING"}
							</Button>
						</Col>
						<Col sm={4}>
							<Button
								color="danger"
								title="Edit"
								onClick={this.props.doDelete}
							>
								Delete
							</Button>
						</Col>
					</Col>
				</Row>
			</ListGroupItem>
		);
	}
}
export default ListItem;
