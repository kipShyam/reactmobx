import React, { Component } from "react";

import { Button, FormGroup, Label, Input, Col, Form } from "reactstrap";
import swal from "sweetalert";
import { inject, observer } from "mobx-react";

@inject("TasksStore")
@observer
class AddTodo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			todo: "",
			currentTask: ''
		};
	}

	componentDidMount() {
		if (this.props.match.params.id) {
			this.getTodoById(this.props.match.params.id);
		}
	}

	getTodoById = taskId => {
		this.props.TasksStore.getTodoById(taskId).then(res => {
			this.setState({
				todo: res.data.name,
				currentTask: res.data
			});
		})
		.catch(ex => {
			console.log(ex);
		})
	};

	onSubmit = event => {
		event.preventDefault();
		if (!this.state.todo) {
			swal("Empty", "Enter Some Todo", "error");
			return;
		}
		this.addTodo(this.state.todo);
	};

	showError = data => {
		swal("Something went wrong!", data, "error");
	};

	showSuccess = data => {
		swal(data.title, data.message, "success").then(() => {
			this.props.history.goBack();
		});
	};

	addTodo = todo => {
		if (this.props.match.params.id) {
			this.props.TasksStore.updateTasks(this.state.todo, this.state.currentTask)
				.then(res => {
					this.showSuccess({
						title: "Todo Updated!",
						message: ""
					});
				})
				.catch(ex => this.showError(JSON.stringify(ex.data)));

		} else {
			this.props.TasksStore.addTasks(this.state.todo)
				.then(res => {
					this.showSuccess({
						title: "Todo Added!",
						message: ""
					});
				})
				.catch(ex => this.showError(JSON.stringify(ex.data)));
		}
	};

	onChange = event => {
		this.setState({ todo: event.target.value });
	};

	render() {
		return (
			<Form className="App" onSubmit={this.onSubmit}>
				<FormGroup row>
					<Label sm={1}>Todo</Label>
					<Col sm={9}>
						<Input sm={10} placeholder="Todo"
						value={this.state.todo}
						onChange={this.onChange} />
					</Col>
					<Button sm={2} color="primary">
						{this.props.match.params.id ? "Update" : "Submit"}
					</Button>
				</FormGroup>
			</Form>
		);
	}
}

export default AddTodo;
