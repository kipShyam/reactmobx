import React, { Component } from "react";
import "./../App.css";
import { inject, observer } from "mobx-react";
import {
	Container,
	ListGroup,
	Button,
	Row,
	ButtonToolbar,
	ButtonGroup
} from "reactstrap";
import { Link } from "react-router-dom";
import ListItem from "./ListItem";
import swal from "sweetalert";
import { STATUS } from "../constants/constants";

@inject("TasksStore")
@observer
class HomePage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			status: STATUS.ALL
		};
		this.updateStatus = this.updateStatus.bind(this);
	}

	componentDidMount() {
		this.props.TasksStore.getTasks();
	}

	deleteItem = task => {
		swal({
			title: "Are you sure?",
			text: "Once deleted, you will not be able to recover selected todo!",
			icon: "warning",
			buttons: ["Cancel", true],
			dangerMode: true
		}).then(willDelete => {
			if (willDelete) {
				this.props.TasksStore.deleteTasks(task);
			}
		});
	};

	updateStatus = status => {
		this.setState({
			status: status
		});
	};

	todoToDisplay = () => {
		return this.props.TasksStore.todo.tasks.filter(tasks => {
			switch (this.state.status) {
				case STATUS.PENDING:
					return tasks.status === STATUS.PENDING;
				case STATUS.COMPLETED:
					return tasks.status === STATUS.COMPLETED;
				default:
					return true;
			}
		});
	};

	toggleCompleted = task => {
		this.props.TasksStore.toggleTaskStatus(task);
	};

	render() {
		return (
			<Container>
				<Row className="form-group">
					<Link to="/add">
						<Button className="mr-15 pull-right" color="primary">
							Add Todo
						</Button>
					</Link>
					<ButtonToolbar className="mr-15 pull-right">
						<ButtonGroup>
							<Button
								onClick={() => this.updateStatus(STATUS.ALL)}
								color={
									this.state.status === STATUS.ALL ? "primary" : ""
								}
							>
								All
							</Button>
							<Button
								onClick={() => this.updateStatus(STATUS.COMPLETED)}
								color={
									this.state.status === STATUS.COMPLETED
										? "primary"
										: ""
								}
							>
								Completed
							</Button>
							<Button
								onClick={() => this.updateStatus(STATUS.PENDING)}
								color={
									this.state.status === STATUS.PENDING ? "primary" : ""
								}
							>
								Pending
							</Button>
						</ButtonGroup>
					</ButtonToolbar>
				</Row>
				<ListGroup>
					{this.todoToDisplay().map((item, index) => (
						<ListItem
							items={item}
							key={index}
							index={index}
							doDelete={() => {
								this.deleteItem(item);
							}}
							toggleCompleted={() => {
								this.toggleCompleted(item);
							}}
						/>
					))}
				</ListGroup>
			</Container>
		);
	}
}

export default HomePage;
